# NARM Map

This tool was designed to extract member institution locations from the official [North American Reciprocal Association](https://narmassociation.org/) (NARM) website and places them into KML format. There are a few Google maps versions of this data created by individuals out there (like [this one](https://patricklundquist.com/2022/03/26/rap-roam-narm-reciprocal-membership-map/)) that are probably mostly accurate, but the reciprocal agreements evidently update at least annually, which means they have a higher potential for inaccuracy every year they are not updated. Hopefully that dynamism within the NARM locations makes this script a particularly useful one, as (in theory) until NARM makes a major update to their website source code, this scraper will always produce a kml file with the most updated map at any time of any year.

## Dependencies
* Python 3.x
    * Requests module for making https requests
    * Beautiful Soup 4.x (bs4) for scraping 
    * Simplekml for easily building KML files
* Also of course depends on the official [Find A NARM Member Institution](https://narmassociation.org/members/) web application.
