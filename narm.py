#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml

#Set filename/path for KML file output
kmlfile = "narm.kml"
#Set KML schema name
kmlschemaname = "narm"
#Set page URL with a 3000 mile radius search from the center of the US
pageURL = "https://narmassociation.org/members/?loc=66604&r=3000"

#Returns reusable BeautifulSoup of a given site
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of location names, addresses, and lat/lng for a given site url
def getlocations():
    #Initialize the soup
    soup = getsoupinit()
    #Initialize locations list
    locations=[]
    #Get the relevant divs, identified by the narm-map-maker class
    divs = soup.find_all('div', class_="narm-map-marker")
    #Loop through all these divs (1 per location)
    for div in divs:
        #Get the name of the location
        name = div.h6.get_text()
        #For the address, we need the second child div
        locationaddress = div.find_all('div')[1].get_text()
        #Use the value of the data-lat and data-lng attributes for the lat/lng data
        lat = div['data-lat']
        lng = div['data-lng']
        #Append the 4 data points collected here into the array associated with this location
        locations.append([name,locationaddress,lat,lng])
    return locations

#Saves a KML file of a given list of locations
def createkml(locations):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each location
    for location in locations:
        #Get the name of the location
        locationname = str(location[0])
        #Get coordinates of the location
        lat = location[2]
        lng = location[3]
        #First, create the point name and description (the address) in the kml
        point = kml.newpoint(name=locationname,description=str(location[1]))
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getlocations())
